#!/bin/sh

# Build the Docker image
docker build -t registry.gitlab.com/testifysec/demos/gitlab-end-to-end/builder:latest .

# Login to the GitLab Container Registry
docker login registry.gitlab.com

# Push the Docker image to GitLab Container Registry
docker push registry.gitlab.com/testifysec/demos/gitlab-end-to-end/builder:latest
