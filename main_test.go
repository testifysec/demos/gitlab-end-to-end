package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rec := httptest.NewRecorder()

	helloWorld(rec, req)

	if status := rec.Code; status != http.StatusOK {
		t.Errorf("Unexpected status code: got %v want %v",
			status, http.StatusOK)
	}
}
