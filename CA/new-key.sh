#!/bin/bash

#working directory
WORKDIR=$PWD

#script directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#change to script directory
cd $DIR



#
# Set CA attributes
COUNTRY="US"
STATE="California"
LOCALITY="San Francisco"
ORGANIZATION="Example CA"
ORGANIZATIONAL_UNIT="Example CA Department"
COMMON_NAME="Example Code Signing Certificate"
EMAIL="codesign@example.com"
BUILD_STEP_NAME=$CI_PIPELINE_URL

#Generate a private key for the certificate
openssl genpkey -algorithm RSA -out codesign.key

#Generate a certificate signing request
openssl req -key codesign.key -new -out codesign.csr -subj "/C=$COUNTRY/ST=$STATE/L=$LOCALITY/O=$ORGANIZATION/OU=$ORGANIZATIONAL_UNIT/CN=$COMMON_NAME/emailAddress=$EMAIL"

#Add a URI extension to the certificate signing request
echo "subjectAltName=URI:spiffe://example.com/$BUILD_STEP_NAME" > codesign_ext.cnf
echo "basicConstraints=critical,CA:FALSE" >> codesign_ext.cnf
openssl req -in codesign.csr -out codesign.csr -config codesign_ext.cnf

#Sign the certificate using the CA
openssl x509 -req -in codesign.csr -CA ca.crt -CAkey ca.key -days 999 -out codesign.crt -extfile codesign_ext.cnf -set_serial 999

cd $WORKDIR