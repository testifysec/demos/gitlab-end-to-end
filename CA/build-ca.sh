#!/bin/bash

# Set CA attributes
COUNTRY="US"
STATE="California"
LOCALITY="San Francisco"
ORGANIZATION="Example CA"
ORGANIZATIONAL_UNIT="Example CA Department"
COMMON_NAME="Example CA"
EMAIL="ca@example.com"

# Generate a private key for the CA
openssl genpkey -algorithm RSA -out ca.key

# Generate a self-signed certificate for the CA
openssl req -key ca.key -new -x509 -out ca.crt \
    -subj "/C=$COUNTRY/ST=$STATE/L=$LOCALITY/O=$ORGANIZATION/OU=$ORGANIZATIONAL_UNIT/CN=$COMMON_NAME/emailAddress=$EMAIL" \
    -days 3650
